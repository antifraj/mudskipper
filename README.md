# the mudskipper project development has ended

Our project was started at the end of 2019 to develop a simple, inexpensive, easy to understand open-source platform for studying decentralized long-range mesh communications, and was successfully used in several hands-on presentations at the beginning of 2020. 

With the emergence of the COVID-19 pandemic and ensuing restrictions on travel and gatherings, we had no choice but to place the project on hold. 

However, in the last 10 months, the decentralized space has kept growing and evolving, and there are now other projects that better achieve the original goals of the Mudskipper project. We, the author and contributors of this project, feel that our time and resources would be better employed supporting some of these emerging projects instead. 

Thanks to everyone who downloaded, installed and supported our little project, but now it’s time to say goodbye.

## where to from here?

Well, it really depends what you’re looking for.

- if you’re already invested in the Ripple protocol and network, and you don’t mind closed source software, the original Ripple project is this way: https://github.com/spleenware/ripple

- if you want a decentralized mesh network that “just works” out of the box, has near worldwide regulatory approvals, is beautifully productized and has the largest network effect, and once again don’t mind the closed source platform, then you’ll love GoTenna Mesh: https://gotennamesh.com/

## still here? okay...

If you won’t settle for anything less than a 100% open source mesh messenger (hardware, firmware, software and API) and want to get involved in a fun project with no political points of failure, fast moving development and a vibrant community, jump over to https://www.meshtastic.org/ . 
**See you there!**

original README file is below this line

---

# mudskipper

An open-source radio mesh messenger using the [Ripple mesh protocol](https://github.com/spleenware/ripple/) over LoRa radio.

Based on [published specs](https://github.com/spleenware/ripple/wiki/Serial-Protocol) and additional clarifications graciously provided by [Ripple author Scott Powell](https://spleenware.wordpress.com/). Not otherwise affiliated or supported by the Ripple project.

In general, this project aims to be fully interoperable with the original Ripple firmware, while serving as a testbench for experimentation and development of new features.
Sections indicated as `NONSTANDARD` are not part of the original Ripple specification.

## hardware requirements

ESP32 CPU, RFM95W radio.

For development and testing, we use the open-source [Sparkfun WRL-15006](https://github.com/sparkfun/ESP32_LoRa_1Ch_Gateway) design, commercially available as ["ESP32 Lora Gateway"](https://www.sparkfun.com/products/15006). 
The firmware should be compatible with most similar boards, as long as the GPIO pin definitions are configured in the source code in accordance with the board layout 

**SAFETY WARNING:** This is proof-of-concept grade software, controlling an RF energy emitting device. Play safe, stay safe.

---
Copyright 2020 by antifraj, MIT licensed (LICENSE for details)
