// mudskipper //
/*
   An open-source radio mesh messenger using Ripple mesh protocol
     https://github.com/spleenware/ripple/
   Based on published specs and additional clarifications
     graciously provided by Ripple author Scott Powell.
   Not otherwise affiliated or supported by the Ripple project.

   In general, this project aims to be fully interoperable with
     the original Ripple firmware, while serving as a testbench
     for experimentation and development of new features.
   Sections indicated as NONSTANDARD are not part of the original
     Ripple specification.

   SAFETY WARNING:
   This is proof-of-concept grade software, controlling an RF
     energy emitting device. Play safe, stay safe.

   Copyright 2020 by antifraj, MIT licensed (LICENSE for details)
     https://gitlab.com/antifraj/
*/

#include <Speck.h>
#include <SpeckSmall.h>
#include <LoRa.h>
#include <TimeLib.h>

/*
   USER-CONFIGURABLE SETTINGS
*/
// Choose ONLY ONE ; if both selected, BLE takes precedence
// Enable Bluetooth-Serial
#define BTS_ENABLED 0
// Enable Bluetooth-LE
#define BLE_ENABLED 1

// Use EEPROM to persist essential settings
#define USE_EEPROM 1
#define NVCFGSAVEINT 3600   // Autosave every X seconds

// HARDWARE SETTINGS
// Settings for Sparkfun WRL-15006 (ESP32 Lora Gateway):
const int pinCS  = 16;  // LoRa radio chip select
const int pinRST = 27;  // LoRa radio reset
const int pinIRQ = 26;  // Hardware interrupt pin

// Settings specific to the Ripple protocol
byte myNodeID = 0x00;   // address of this node
byte myUserID = 0x01;   // address of this user
byte groupID  = 0x01;   // Ripple group ID

// LoRa radio settings for Ripple network
int loraFR = 915E6;     // Frequency (can be set via API)
int loraSF = 10;        // Spreading - mandatory fixed
int loraBW = 250000;    // Bandwidth - mandatory fixed
int txPower = 17;       // TX power level

// Settings impacting memory usage
#define INBOX_SIZE 64   // Max number of items in receive queue
#define NODES_SIZE 64   // Max items in neighbours list

// Show verbose op/diag messages on serial console
// (hash-prefixed to avoid conflict with serial API)
#define VERBOSECONSOLE 1

/*
   END OF USER-CONFIGURABLE SETTINGS
*/


// Software version
#define VMAJ 0
#define VMIN 1
#define VREV 4
#define PROTOVERSION 1
#define NVCFGVERSION 1

// Some aggregate metrics, for the marketing department :->
unsigned long lastSendTime = 0;       // last send time
unsigned long lastRecvTime = 0;       // last recv time
unsigned long sentCount = 0;          // count of sent messages
unsigned long recvCount = 0;          // count of rcvd messages

// Packet buffers, probably one too many

byte loraBuf[256];    // LORA packet buffer
int loraLen;

byte clearBuf[256];   // Post-decryption cleartext buffer
int clearLen;

byte clearOutBuf[256];  // Pre-encryption cleartext buffer
int clearOutLen;

// Encryption cipherbox definitions
#define SPECK_FLAVOUR SpeckSmall
#define KEYLENGTH 32  // bytes

// (NONSTANDARD) Global broadcast cipherbox
SPECK_FLAVOUR globalCipherBox;

// (NONSTANDARD) The built-in encryption key for global broadcasts
// HARD-CODED in every device to enable basic functionality from cold boot,
//   can be changed via (nonstandard) API call f255,{new_key}
byte globalKey[KEYLENGTH] = {
  0x0C, 0xC5, 0xDB, 0xAE, 0x28, 0x55, 0xA8, 0xAE,
  0xE1, 0xF2, 0x51, 0xFF, 0xD8, 0x22, 0x30, 0x04,
  0x23, 0x72, 0x57, 0x60, 0x8B, 0xB5, 0x2C, 0x91,
  0xC4, 0x0E, 0x88, 0x17, 0x3D, 0xF4, 0x9E, 0x51
};

// Super simple debug output - prefix with a hash and dump to serial
// (NONSTANDARD) Apps using the Serial API should ignore hash-ed lines
void debugOutput(String val) {
#if VERBOSECONSOLE
  Serial.print("# " + val + "\n");
#endif
}

#if USE_EEPROM
#include <EEPROM.h>
#define EEPROM_SIZE 64
#endif

#if BTS_ENABLED
#include "BluetoothSerial.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif
BluetoothSerial SerialBT;
#endif

#if BLE_ENABLED
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool bleDeviceConnected = false;
bool bleOldDevConnected = false;
uint8_t txValue = 0;

// UART service UUID
#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

// max size of BLE TX packets
#define BLE_TX_SIZE 20

String bleCmdBuf = "";
String apiCmdBuf = "";
String apiResBuf = "";

String apiCommand(String cmd);

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      bleDeviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      bleDeviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
      if (rxValue.length() > 0) {
        debugOutput("BLE[RX]:" + String(rxValue.length()));
        for (int i = 0; i < rxValue.length(); i++) {
          if (rxValue[i] == '\n') {
            // load command into execution buffer
            //   if buffer busy, just ignore command
            if (apiCmdBuf.length() == 0) {
              apiCmdBuf = bleCmdBuf;
            } else {
              debugOutput("BLE API cmd LOST due to buffer busy: " + bleCmdBuf);
            }
            bleCmdBuf = "";
          } else {
            bleCmdBuf += rxValue[i];
          }
        }
      }
    }
};
#endif


/*
   INBOX is where type M and type A messages are queued
     for collection by app. Once more, this is a minimal
     approach and should be further refined for any
     production purposes.
*/
struct inboxMsg
{
  unsigned int tstamp;
  unsigned int msgid;  // if type=A ACKID, or if type=M msgID field
  int rssi;
  // 4 bytes together keep struct 32-bit aligned
  byte sender;  // change here when extending sender ID size
  byte origin;
  byte type;
  byte length;
  // end of 4 bytes
  byte body[240];
};
inboxMsg inbox[INBOX_SIZE]; // defined above in user section
int inboxHead = 0;

#if USE_EEPROM
// non-volatile configuration block
struct nvConfigV01
{
  byte filever;       // config file version (NVCFGVERSION)
  byte reserve = 0;   // reserved
  byte userID = 0xff; // user ID
  byte groupID = 1;   // group ID
  uint32_t freqKHz;   // frequency in kHz
  uint32_t fakeRTC;   // fake hardware clock
  byte crc;           // CRC (using CRC8-MAX algo)
};
nvConfigV01 nvConfig;

// don't use sizeof, just count bytes manually (for alignment reasons)
#define NVCFGSIZE 13
#endif

/*
   UTILITY FUNCTIONS
*/
byte CRC8_add(byte b, byte crc = 0) {
  for (int tempI = 8; tempI; tempI--) {
    byte sum = (crc ^ b) & 0x01;
    crc >>= 1;
    if (sum)
      crc ^= 0x8C;
    b >>= 1;
  }
  return crc;
}

unsigned long CRC32_add(unsigned long b, unsigned long crc = 0xFFFFFFFF) {
  unsigned long mask;
  crc = crc ^ b;
  for (int tempI = 8; tempI; tempI--) {    // Do eight times.
    mask = -(crc & 1);
    crc = (crc >> 1) ^ (0xEDB88320 & mask);
  }
  return crc;
}


// HEX Dumps a 16-byte block. Format as follows:
//   0 = "hexlify"
//   1 = "hex-only"
//   2 = "hex+text"

String hexDump16(byte *data, byte format = 0)
{
  char sprbuf[4];
  char xxdbuf[16];
  String strData;

  for (int tempI = 0; tempI < 16; tempI++) {
    sprintf(sprbuf, "%02x", data[tempI]);
    strData += String(sprbuf);
    if (format > 0) strData += " ";
  }

  if (format == 2) {
    strData += "| ";
    for (int tempI = 0; tempI < 16; tempI++) {
      if (isPrintable(data[tempI]))
        strData += (char) data[tempI];
      else
        strData += ".";
    }
  }
  return strData;
}

// Convert a HEX string to bytes, like Python unhexlify
//   (limited at 240 bytes for sanity)

int hexToBytes(byte *hexes, byte *bytes, int bytecount)
{
  byte * bp;
  byte sb[4] = {0, 0, 0, 0};

  memset(bytes, 0, 256);
  memset(sb, 0, 4);
  if (bytecount > 240)
    bytecount = 240;

  for (int tempI = 0; tempI < bytecount; tempI++) {
    // Process in groups of two chars -> 1 byte
    memcpy(sb, hexes + 2 * tempI, 2);
    bytes[tempI] = strtoul((char *)sb, NULL, 16) & 0xFF;
  }

  return 0;
}


void setup()
{
  Serial.begin(9600);     // initialize serial console
  while (!Serial);        // recommended if USB serial

  delay(500);

#if USE_EEPROM
  nvLoad();
#endif

#if BTS_ENABLED
  SerialBT.begin("mudskipper-bt");  // Bluetooth device name
  debugOutput("BT Serial device started.");
#endif

#if BLE_ENABLED
  // Create the BLE Device
  BLEDevice::init("mudskipper-le");  // BLE device name

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                        CHARACTERISTIC_UUID_TX,
                        BLECharacteristic::PROPERTY_NOTIFY
                      );

  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RX,
      BLECharacteristic::PROPERTY_WRITE
                                          );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  debugOutput("BLE service started.");
#endif

  // Configure the CS, reset, and IRQ pins
  LoRa.setPins(pinCS, pinRST, pinIRQ);

  if (!LoRa.begin(loraFR)) {   // init radio at default frequency
    debugOutput("LoRa init failed. Check connections.");
    while (true);              // if failed, do nothing
  }
  LoRa.setSignalBandwidth(loraBW);
  LoRa.setSpreadingFactor(loraSF);
  LoRa.setTxPower(txPower);
  debugOutput("LoRa init succeeded.");

  // (NONSTANDARD) set up broadcast key
  globalCipherBox.setKey(globalKey, KEYLENGTH);

  // reset mesh node ID and parameters
  rippleNodeReset();
}


void loop()
{
  String apiOut = "";
  char rx_byte = 0;

  String serInput = "";
  while (Serial.available() > 0) {    // is a character available?
    rx_byte = Serial.read();       // get the character
    if (rx_byte == '\n') {
      apiOut = apiCommand(serInput);
      if (apiOut.length() > 0)
        Serial.print(apiOut + "\n");
      serInput = "";
    } else {
      serInput += (char) rx_byte;
    }
  }

#if BTS_ENABLED
  String btsInput = "";
  while (SerialBT.available()) {
    rx_byte = SerialBT.read();       // get the character
    if (rx_byte == '\n') {
      apiOut = apiCommand(btsInput);
      if (apiOut.length() > 0)
        SerialBT.print(apiOut + "\n");
      btsInput = "";
    } else {
      btsInput += (char) rx_byte;
    }
  }
#endif

#if BLE_ENABLED
  // any command in the BLE buffer? execute it now
  if (apiCmdBuf.length() > 0) {
    debugOutput("BLE -> API, L=" + String(apiCmdBuf.length()));

    apiResBuf = apiCommand(apiCmdBuf);
    if (apiResBuf.length() > 0) {
      apiResBuf += "\n";
      debugOutput("BLE <- API, L=" + String(apiResBuf.length()));

      // Packetize output into BLE_TX_SIZE blocks
      for (int tempI = 0; tempI < apiResBuf.length(); tempI += BLE_TX_SIZE) {
        delay(10);
        if ((tempI + BLE_TX_SIZE) <= apiResBuf.length()) {
          // full packet
          pTxCharacteristic->setValue((byte *)(apiResBuf.c_str() + tempI), BLE_TX_SIZE);
          debugOutput("BLE[TX]:" + String(BLE_TX_SIZE));
        } else {
          // last packet
          pTxCharacteristic->setValue((byte *)(apiResBuf.c_str() + tempI), apiResBuf.length() % BLE_TX_SIZE);
          debugOutput("BLE[TX]:" + String(apiResBuf.length() % BLE_TX_SIZE));
        }
        pTxCharacteristic->notify();
      }
    }
    // done, free BLE buffer for next command
    apiCmdBuf = "";
  }

  // disconnecting
  if (!bleDeviceConnected && bleOldDevConnected) {
    debugOutput("BLE Disconnecting");
    delay(500); // give the bluetooth stack the chance to get things ready

    pServer->startAdvertising(); // restart advertising
    debugOutput("BLE Start Advertising");
    bleOldDevConnected = bleDeviceConnected;
  }

  // connecting
  if (bleDeviceConnected && !bleOldDevConnected) {
    debugOutput("BLE Connecting");
    bleOldDevConnected = bleDeviceConnected;
  }
#endif

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());

#if USE_EEPROM
  // Save configuration every save interval
  if ((now() - nvConfig.fakeRTC) > NVCFGSAVEINT)
    nvSave();   // will also update fakeRTC field
#endif
}


/*
   INBOUND PACKET WORKFLOW:

   - check CRC8 to filter out foreign/corrupt packets
   - record/update sender node in neighbours list
   - check and reject duplicate objects (normal in a mesh network)
   - process object locally (decrypt, ack, save to inbox etc)
   - relay object to mesh network
 */
void onReceive(int packetSize)
{
  if (packetSize == 0)
    return;

  // Prepare reception buffer
  memset (loraBuf, 0, 256);
  loraLen = 0;

  // Receive a full Lora packet
  while (LoRa.available()) {
    byte read_one = LoRa.read();
    loraBuf[loraLen] = (byte)read_one;
    loraLen++;
  }

  // Record radio conditions
  int RSSI = LoRa.packetRssi();
  float SNR = LoRa.packetSnr();

  // Increment global counter (currently unused)
  lastRecvTime = now();
  recvCount++;

  debugOutput("LORA[RECV] L=" + String(loraLen) +
              " RSSI=" + String(RSSI) + " SNR=" + String(SNR));

  // CRC8 check to reject non-Ripple packets
  if (rippleVerifyCRC(loraBuf, loraLen) != 0) {
    debugOutput("CRC failed, corrupt or non-Ripple packet!");

    // Let's examine the packet for corruption
    debugOutput(hexDump16(loraBuf, 2));
    if (loraLen > 32) {
      debugOutput(hexDump16(loraBuf + 16, 2));
    }
    return;   // can't continue
  }

  // Update table of seen nodes
  //   (do this before duplicate rejection)
  byte sender = loraBuf[2];       // sender ID
  rippleMeshSeenNode(sender, RSSI);

  // Duplicate rejection
  if (rippleSeenObject(loraBuf + 3, loraLen - 3)) {
    debugOutput("Object SEEN before, discarding");
    return;
  }

  // Local object handler
  rippleRecvRaw(loraBuf, loraLen, RSSI, SNR);

  // Mesh network relay function
  rippleRelay(loraBuf, loraLen, RSSI, SNR);

}


/************************************************************
   1) Addressbook/cipherbook operations

   Addressbook maps friend IDs to encryption cipherboxes.
   Implemented as a ring list, old records are overwritten by new ones
   Probably not the best solution, but OK in our minimalist scenario
     (IDs are metadata and metadata is toxic)
 ************************************************************/
#define ABOOK_SIZE 24 // max number of entries (new entries overwrite old!)
#define AB_FLAG_SENDACKS (1 << 0)  // do we send ACK packets to this ID?
#define AB_FLAG_WANTACKS (1 << 1)  // do we expect ACK packets? 
struct Friend
{
  int id = 0xff;
  SPECK_FLAVOUR cipherBox;
  int flags = AB_FLAG_SENDACKS | AB_FLAG_WANTACKS;  // explained above
  unsigned long rxCount = 0;
  unsigned long txCount = 0;
};
Friend aBook[ABOOK_SIZE];
int aBookHead = 0;

/* Address book Find ID - find friend by origin ID
*/
int aBookFindID (int originID)
{
  int ix = -1;
  for (int tempI = 0; tempI < ABOOK_SIZE; tempI++) {
    if (aBook[tempI].id == originID) {
      ix = tempI;
      debugOutput("Found ID " + String(originID) + " at IX: " + String(tempI));
      break;
    }
  }
  return ix;
}

/* Address book Set Key - update or create a friend cipherbox
*/
void aBookSetKey(int originID, byte * key)
{
  int ix = aBookFindID(originID);

  if (ix < 0) {
    // not found; create new entry
    aBook[aBookHead].id = originID;
    ix = aBookHead;
    aBookHead++;
    aBookHead %= ABOOK_SIZE;
  }

  aBook[ix].cipherBox.setKey((byte *) key, 32);
}



/************************************************************
   2) Encryption primitives

   The argument aBookIx is the addressbook index of the
     "friend" to/from whom we are en/de-crypting, and is used
     for selecting the correct cipherbox. See above.
 ************************************************************/
int rippleEncrypt(byte *clearText, byte *cipherText, int textLen, int aBookIx)
{
  // Cipher blocks are always 16 bytes
  int blocks = textLen >> 4;
  if (textLen - (blocks << 4) > 0) blocks++; // one extra block for the remainder

  byte * bufenc;
  byte * bufdec;

  // encryption block-by-block
  for (int block = 0; block < blocks; block++) {
    bufenc = cipherText + 16 * block;
    bufdec = clearText + 16 * block;

    memset(bufenc, 0, 16);
    if (aBookIx < 0xff) {
      // Use specific cipherbox
      aBook[aBookIx].cipherBox.encryptBlock(bufenc, bufdec);
    } else {
      // Use system-wide broadcast cipherbox
      globalCipherBox.encryptBlock(bufenc, bufdec);
    }

    debugOutput("CLR: " + hexDump16(bufdec, 2));
    debugOutput("ENC: " + hexDump16(bufenc, 2));
  }
  return (blocks << 4);
}

int rippleDecrypt(byte *cipherText, byte *clearText, int textLen, int aBookIx)
{
  // Cipher blocks are always 16 bytes
  int blocks = textLen >> 4;

  byte * bufenc;
  byte * bufdec;

  memset(clearText, 0, textLen);

  // decryption block-by-block
  for (int block = 0; block < blocks; block++) {
    bufenc = cipherText + 16 * block;
    bufdec = clearText + 16 * block;

    if (aBookIx < 0xff) {
      // Use specific cipherbox
      aBook[aBookIx].cipherBox.decryptBlock(bufdec, bufenc);
    } else {
      // Use system-wide broadcast cipherbox
      globalCipherBox.decryptBlock(bufdec, bufenc);
    }
    debugOutput("ENC: " + hexDump16(bufenc, 2));
    debugOutput("DEC: " + hexDump16(bufdec, 2));
  }
  return (blocks << 4);
}


/************************************************************
   3) Ripple protocol object operations

      rippleVerifyCRC - validate received packet
      rippleRecvRaw   - local process received packet
      rippleSendRaw   - send a packet via LoRa
 ************************************************************/

// Node descriptor - used to store info on neighbouring nodes
struct NodeDescriptor
{
  byte nodeID;
  int RSSI = 0;
  unsigned long tstamp = 0;
};
// Ring list of seen nodes - used by mesh relay
NodeDescriptor nodeList[NODES_SIZE];
int nodeHead = 0;

// Ring list of CRC32's of seen messages - used by mesh relay
unsigned long seenList[64];
int seenHead = 0;

/*
   Calculate and verify the packet CRC8, which qualifies
     the inbound Lora packet as a valid Ripple packet

   "(...) the crc is calculated like so:
     CRC8 crc;
     crc.update(groupId);  // seed with group-id
     crc.update(origin);
     crc.update(sender);
     crc.update(buf, len);  // the remaining part of packet"
*/
int rippleVerifyCRC(byte * recvBuf, int recvLen)
{
  // packet header bytes:
  byte want_crc = recvBuf[0];     // CRC8

  byte have_crc = 0;
  have_crc = CRC8_add(groupID, have_crc);
  for (int tempI = 1; tempI < recvLen; tempI++) {
    have_crc = CRC8_add(recvBuf[tempI], have_crc);
  }

  // CRC8 check
  if (have_crc != want_crc) {
    debugOutput("CRC failed: WANT=0x" + String(want_crc, HEX) +
                ", HAVE=0x" + String(have_crc, HEX));
    return -1;
  }
  return 0;
}

// Local handling of rcvd packets according to Ripple protocol

void rippleRecvRaw(byte * recvBuf, int recvLen, int RSSI, float SNR)
{
  /*
     Ripple packet format:

         aabbccxxxx...xx
         | | | |
         | | | data blob (usually encrypted)
         | | sender ID (last hop node ID)
         | origin ID (user ID that originated message)
         CRC8 (for packet validation, algo above)

     Data blob can be:
      - a message delivery acknowledgment (4 byte integer, cleartext)
      - an encrypted payload (n * 16 byte blocks, Speck-256 algo)
  */
  unsigned int msg32bitID = 0;

  // packet header bytes:
  byte want_crc = recvBuf[0];     // CRC8
  byte origin = recvBuf[1];       // origin ID
  byte sender = recvBuf[2];       // sender ID

  // mini buffer for sprintf
  char sprbuf[4];

  unsigned int blobLen = (recvLen - 3);
  // Cipher blocks are always 16 bytes
  int blocks = blobLen >> 4;

  // if packet is ours, print details:
  debugOutput(" CRC8  : 0x" + String(want_crc, HEX));
  debugOutput(" Origin: 0x" + String(origin, HEX));
  debugOutput(" Sender: 0x" + String(sender, HEX));
  debugOutput(" Length: " + String(blobLen));
  debugOutput(" Blocks: " + String(blocks));
  debugOutput(" RSSI  : " + String(RSSI));
  debugOutput(" SNR   : " + String(SNR));

  // the blob length is first indication of object type
  if (blobLen == 4) {
    // This is an ACK packet, not encrypted
    // "The ACK packets have origin = 0xFF, then just have 32-bit little-endian id"
    for (int tempI = 0; tempI < 4; tempI++)
      msg32bitID += (recvBuf[3 + tempI] << (8 * tempI));

    debugOutput("ACK ID: " + String(msg32bitID, HEX));
    // A:{checksum}
    // push to inbox
    if (inboxHead < INBOX_SIZE) {
      inbox[inboxHead].type = 'A';
      inbox[inboxHead].tstamp = now();
      inbox[inboxHead].msgid = msg32bitID;
      inbox[inboxHead].rssi = RSSI;
      inbox[inboxHead].length = 0;
      inbox[inboxHead].origin = origin;
      inbox[inboxHead].sender = sender;
      debugOutput("Saved to INBOX at pos " + String(inboxHead));
      inboxHead++;
      apiNotify("T:");
    } else {
      debugOutput("INBOX full, can't save message");
    }
    return;    // all done

  } else if ((blobLen - (blocks << 4)) > 0) {
    // Sanity check - whole number of blocks
    debugOutput("Invalid length (not n*16)");
    return;    // no further processing

  } else {
    // Length is correct, proceed to decryption step
    if ( origin == 0xff ) {
      // Our special case
      rippleDecrypt(recvBuf + 3, clearBuf, blobLen, 0xff);
    } else {
      // Find entry in addressbook
      int ix = aBookFindID(origin);
      if ( ix < 0 ) {
        debugOutput("Origin " + String(origin) + " not in contacts list");
        rippleDecrypt(recvBuf + 3, clearBuf, blobLen, 0xff); // a desperate attempt
      } else {
        rippleDecrypt(recvBuf + 3, clearBuf, blobLen, ix); // use existing entry
      }
    }

    /*
       Ripple payload formats:

       DATAGRAM:

           0100ttttttttxxxx...xx
           | | |       |
           | | |       binary data
           | | epoch timestamp
           | reserved - must be 0, see below
           payload type: 01=datagram

       TEXT MESSAGE:

           0800aaaaaaaabbxxxx...xx
           | | |       | |
           | | |       | c-string body (\0 ends)
           | | |       resend counter
           | | message ID (usu. epoch time)
           | reserved - must be 0, see below
           payload type: 08=text message
    */

    /* Sanity check on decrypted payload (Scott P email)
       "the first byte is type (...) Second byte must be 0"
       "Just the two types supported at present"
    */
    if (clearBuf[1] != 0) {
      debugOutput("Decryption FAILURE suspected (byte 1 >0)");
      return;      // can't continue
    }

    // Calculate ACK ID for incoming message
    unsigned long ackCRC32 = 0xffffffff;
    ackCRC32 = CRC32_add(origin, ackCRC32);
    for (int tempI = 0; tempI < blobLen - 2; tempI++) {
      // Important! stop early if end of string \x00 encountered
      //   but not while processing the binary header
      if ((tempI > 5) && (clearBuf[tempI + 2] == 0))
        break;
      ackCRC32 = CRC32_add(clearBuf[tempI + 2], ackCRC32);
    }
    ackCRC32 = ~ackCRC32;

    if (clearBuf[0] == 1) {
      // datagram, immediate delivery to API
      debugOutput("Type=DGR length=" + String(blobLen - 6));
      // D:{from},{rssi},{hex-bytes-payload}
      String dgrNotif = "D:" + String(origin) + "," + String(LoRa.packetRssi()) + ",";
      for (int tempI = 6; tempI < blobLen; tempI++) {
        sprintf(sprbuf, "%02x", clearBuf[tempI]);
        dgrNotif += String(sprbuf);
      }
      // unsolicited notification
      apiNotify(dgrNotif);

    } else if (clearBuf[0] == 8) {
      // storable message, deliver to inbox
      debugOutput("Type=MSG length=" + String(blobLen - 7));

      for (int tempI = 0; tempI < 4; tempI++)
        msg32bitID += (clearBuf[2 + tempI] << (8 * tempI));

      debugOutput("MSGID=" + String(msg32bitID));

      // push to inbox
      if (inboxHead < INBOX_SIZE) {
        inbox[inboxHead].type = 'M';
        inbox[inboxHead].tstamp = now();
        inbox[inboxHead].msgid = msg32bitID; // FIXME
        inbox[inboxHead].rssi = RSSI;
        inbox[inboxHead].length = blobLen - 7; // FIXME
        inbox[inboxHead].origin = origin;
        inbox[inboxHead].sender = sender;
        memset(inbox[inboxHead].body, 0, 240);
        memcpy(inbox[inboxHead].body, clearBuf + 7, blobLen - 7);

        debugOutput("Saved to INBOX at pos " + String(inboxHead));
        inboxHead++;

        // Send ACK
        debugOutput("Send ACK for ACK_ID:" + String(ackCRC32, HEX));
        rippleSendAck(ackCRC32);

        apiNotify("T:");
      } else {
        debugOutput("INBOX full, can't save item");
      }
    }
  }
}


/*
   Send an encrypted blob with a Ripple crc/origin/sender header
     (format described above)
*/
void rippleSendRaw(byte origin, byte sender, byte * payload, int payLen)
{
  // calculate CRC8-MAX using Ripple method
  byte my_crc = 0;
  my_crc = CRC8_add(groupID, my_crc);
  my_crc = CRC8_add(origin, my_crc);
  my_crc = CRC8_add(sender, my_crc);

  for (int tempI = 0; tempI < payLen; tempI++)
    my_crc = CRC8_add(payload[tempI], my_crc);

  // build and send packet for transmission
  int txStart = millis();
  LoRa.beginPacket();               // start packet
  LoRa.write(my_crc);               // add crc8
  LoRa.write(origin);               // add destination address
  LoRa.write(sender);               // add sender address
  LoRa.write(payload, payLen);      // add payload
  LoRa.endPacket();                 // finish packet and send it

  debugOutput("LORA[SEND] L=" + String(payLen + 3) + " completed in " + String(millis() - txStart) + "ms");

  // Add to list of seen objects
  rippleSeenObject(payload, payLen);

  // Increment global counter (currently unused)
  sentCount++;
  lastSendTime = now();
}


/*
   Look up an object in the "seen objects" list, add it to the list if not found
   IMPORTANT: the argument is a direct pointer to the object payload 
     (skipping the 3-byte packet header)
 */
int rippleSeenObject(byte * payloadBuf, int payloadLen) {

  // Calculate object CRC32 for duplicate detection
  // This is not the same as the ACK ID, as here we CRC the ENCRYPTED payload
  unsigned long objCRC32 = 0xffffffff;

  for (int tempI = 0; tempI < payloadLen; tempI++) {
    objCRC32 = CRC32_add((byte) payloadBuf[tempI], objCRC32);
  }
  debugOutput("OBJ_ID: " + String(objCRC32, HEX));

  // Look up object in list of seen objects
  for (int tempI = 0; tempI < 64; tempI++)
    if (objCRC32 == seenList[tempI]) {
      // seen before
      return(1);
      break;
    }

  // Not seen before; add to list now
  seenHead++;
  seenHead %= 64;
  seenList[seenHead] = objCRC32;

  return(0);
}

/************************************************************
   3.5) Ripple protocol node operations

      rippleNodeReset - (re)set mesh node id and state
      rippleMeshRelay - relay rcvd packet on mesh network
 ************************************************************/

/*
   Basically reset/initialize mesh node state
   1) set node id in the mesh network. In the official Ripple firmware this is
     the same as the user id, but is not required to be.
   2) wipe list of seen messages
   ... add more actions as necessary
*/
void rippleNodeReset()
{
  // reset node ID
  myNodeID = random(255);

  // FIXME - look up nodelist to avoid further collision

  debugOutput("SetNodeID: 0x" + String(myNodeID, HEX));

  // reset "seen objects" list
  memset(seenList, 0, 64 * sizeof(unsigned long));
  seenHead = 0;
}


// The Ripple mesh network relay function

void rippleRelay(byte * recvBuf, int recvLen, int RSSI, float SNR)
{
  // packet header bytes:
  byte want_crc = recvBuf[0];     // CRC8
  byte origin = recvBuf[1];       // origin ID
  byte sender = recvBuf[2];       // sender ID

  // length of data excluding header
  int blobLen = (recvLen - 3);

  // Detect node ID conflicts
  if (sender == myNodeID) {
    debugOutput("MESH: NodeID conflict, resetting our end.");
    rippleNodeReset();
  }

  // duplicate object rejection moved to upstream function
  debugOutput("MESH: WILL RELAY this object");
  delay(random(64));  // naive attempt to help collision avoidance

  // preserve origin, insert our node ID as sender; sendRaw will recalc CRC
  rippleSendRaw(origin, myNodeID, recvBuf + 3, blobLen);
}


void rippleMeshSeenNode(byte nodeID, int RSSI)
{
  int found = 0;
  int tempI;
  for (tempI = 0; tempI < NODES_SIZE; tempI++) {
    if ((nodeList[tempI].nodeID == nodeID) && (nodeList[tempI].tstamp > 0)) {
      debugOutput("Node " + String(nodeID, HEX) + " found at pos " + String(tempI) +
                  "; RSSI=" + String(nodeList[tempI].RSSI) + " " + String(now() - nodeList[tempI].tstamp) + "s ago.");
      found = 1;
      break;
    }
  }

  // Push at top of ring list
  nodeList[nodeHead].nodeID = nodeID;
  nodeList[nodeHead].tstamp = now();
  nodeList[nodeHead].RSSI = RSSI;
  debugOutput("Node " + String(nodeID, HEX) + " saved at pos " + String(nodeHead) +
              "; RSSI=" + String(RSSI));
  nodeHead++;
  nodeHead %= NODES_SIZE;

  // delete previous record
  if (found) {
    memset((byte *) &nodeList[tempI], 0, sizeof(NodeDescriptor));
  }
}


/************************************************************
   4) Ripple high-level message functions

      rippleSendMsg - send a p-to-p message
      rippleSendDgm - send a stateless datagram
      rippleSendAck - send a message acknowledgment
 ************************************************************/

unsigned long rippleSendMsg(String outgoing, int aBookIx, unsigned long tstamp, byte retries)
{
  int buflen = 0;

  memset(clearOutBuf, 0, 256);
  // 08 00 -> message indicator
  clearOutBuf[0] = 8;
  clearOutBuf[1] = 0;
  buflen += 2;

  // 32-bit timestamp, convert to bytes
  for (int i = 0; i < 4; i++)
    clearOutBuf[2 + i] = (byte) ((tstamp >> (i * 8)) & 0xff);
  buflen += 4;

  // 1 byte retry counter
  clearOutBuf[6] = retries;
  buflen++;

  for (int tempI = 0; tempI < outgoing.length(); tempI++)
    clearOutBuf[tempI + 7] = (byte) outgoing.charAt(tempI);
  buflen += outgoing.length();

  /*
     Calculate cleartext payload CRC32 for ACK

     "uses the CRC32 Arduino library. First, it is seeded with the
     origin id, then updated with the clear text bytes, skipping the two
     signature bytes at start" (Scott Powell, via email)
  */
  unsigned long ackCRC32 = 0xffffffff;
  ackCRC32 = CRC32_add(myUserID, ackCRC32);
  for (int tempI = 0; tempI < buflen - 2; tempI++)
    ackCRC32 = CRC32_add(clearOutBuf[tempI + 2], ackCRC32);
  ackCRC32 = ~ackCRC32;
  debugOutput("ACK_ID: " + String(ackCRC32, HEX));

  memset(loraBuf, 0, 256);
  debugOutput("Prep buf len: " + String(buflen));
  int outlen = rippleEncrypt(clearOutBuf, loraBuf + 3, buflen, aBookIx);

  rippleSendRaw(myUserID, myNodeID, (byte *) (loraBuf + 3), outlen);
  return (unsigned long) ackCRC32;
}


unsigned long rippleSendDgm(char * outBinBuf, int outBinLen, int aBookIx, unsigned long tstamp)
{
  int buflen = 0;

  memset(clearOutBuf, 0, 256);
  // 01 00 -> datagram indicator
  clearOutBuf[0] = 1;
  clearOutBuf[1] = 0;
  buflen += 2;

  // 32-bit timestamp
  for (int i = 0; i < 4; i++)
    clearOutBuf[2 + i] = (byte) ((tstamp >> (i * 8)) & 0xff); // extract the right-most byte of the shifted variable
  buflen += 4;

  for (int tempI = 0; tempI < outBinLen; tempI++)
    clearOutBuf[tempI + 6] = (byte) outBinBuf[tempI];
  buflen += outBinLen;

  memset(loraBuf, 0, 256);
  debugOutput("Prep buf len: " + String(buflen));
  int outlen = rippleEncrypt(clearOutBuf, loraBuf + 3, buflen, aBookIx);

  rippleSendRaw(myUserID, myNodeID, (byte *) (loraBuf + 3), outlen);
  return 0;
}


unsigned long rippleSendAck(unsigned int ackID)
{
  memset(clearOutBuf, 0, 256);

  for (int i = 0; i < 4; i++)
    clearOutBuf[i] = (byte) ((ackID >> (i * 8)) & 0xff);

  rippleSendRaw(0xff, myNodeID, (byte *) (clearOutBuf), 4);
  return 0;
}


/************************************************************
   5) Serial API implementation

   https://github.com/spleenware/ripple/wiki/Serial-Protocol

     apiCommand  - execute a client command and return result
     apiNotify   - send an unsolicited notification to client
 ************************************************************/

// Called from serial loop to process API commands

String apiCommand(String cmd)
{
  int int_args[4] = { 0, 0, 0, 0 };
  float freqMHz = 0;
  int tempI;
  int parsePos = 0;
  unsigned long ackID = 0;
  byte * bp;
  byte hexbuf[256];
  int hexlen;
  String message;
  String apiResp;

  // Good opportunity to keep an eye on the time
  debugOutput("TIME=" + String(now()));
  debugOutput("API[REQ]: " + cmd);

  switch (cmd.charAt(0)) {
    case 'i':
      // Set Identity
      // i{id},{group-id},{radio-frequency}
      int_args[0] = cmd.substring(parsePos + 1).toInt() & 0xff;
      parsePos = cmd.indexOf(",", parsePos + 1);

      int_args[1] = cmd.substring(parsePos + 1).toInt() & 0xff;
      parsePos = cmd.indexOf(",", parsePos + 1);

      // Frequency is optional, and a float
      freqMHz = cmd.substring(parsePos + 1).toFloat();

      debugOutput("setUID: " + String(myUserID) + "->" + String(int_args[0]));
      debugOutput("setGID: " + String(groupID) + "->" + String(int_args[1]));
      myUserID = int_args[0];
      groupID = int_args[1];

      // Sanity check on frequency
      if (freqMHz > 400) {
        debugOutput("setFrq:" + String(loraFR) + "->" + String(trunc(freqMHz * 1E6)));
        loraFR = int(freqMHz * 1E6);
        LoRa.setFrequency(loraFR);
      }

#if USE_EEPROM
      // save config as relevant data changed
      nvSave();
#endif

      // Identity Response
      // I:{max-message-len},{firmware-ver}
      apiResp = "I:126,1";
      break;

    case 'c':
      // Set Realtime Clock
      // c{UTC-time-in-millis-div-100}
      int_args[0] = cmd.substring(1).toInt();
      int_args[0] /= 10;
      int_args[0] += 1514764800; // ?!! TZ issues? FINDOUT
      setTime(int_args[0]);
      break;

    case 'f':
      // Add or Update a Friend
      // f{id},{hex-shared-secret}
      int_args[0] = cmd.substring(parsePos + 1).toInt() & 0xff;
      parsePos = cmd.indexOf(",", parsePos + 1);
      if (cmd.substring(parsePos + 1).length() != 2 * KEYLENGTH) {
        debugOutput("Invalid key length");
        apiResp = "F:0";
      } else {
        // Prepare a destination buffer for hex conversion
        memset(hexbuf, 0, 256);
        // Point hex converter to beginning of hex string
        bp = (byte *) cmd.c_str() + parsePos + 1;

        hexToBytes(bp, hexbuf, KEYLENGTH);

        if (int_args[0] < 0xff) {
          // we reserve ID 0xff for our non-standard use
          aBookSetKey(int_args[0], hexbuf);
        } else {
          // (NONSTANDARD) Re-key global broadcast cipherbox
          debugOutput("OLDKEY: " + hexDump16(globalKey, 0) + hexDump16(globalKey + 16, 0));
          debugOutput("NEWKEY: " + hexDump16(hexbuf, 0) + hexDump16(hexbuf + 16, 0));
          memcpy(globalKey, hexbuf, KEYLENGTH);
          globalCipherBox.setKey(hexbuf, 32);
        }
        // F:{id} if successful
        apiResp = ("F:" + String(int_args[0]));
      }
      break;

    case 'r':
      // Send a Datagram
      //   r{to-id},{hex-bytes-payload}

      int_args[0] = cmd.substring(parsePos + 1).toInt() & 0xff;
      parsePos = cmd.indexOf(",", parsePos + 1);

      hexlen = int((cmd.length() - parsePos - 1) / 2);

      if (int_args[0] == myUserID) {
        debugOutput("UNHANDLED selfie DGR, L=" + String(hexlen));
        /*
           "The 01 (hex) is the status request. The 02 is the status response with
           battery voltage (in milli volts) as little-endian last two bytes.
           You can fire a 01 to any node to query its status. The app sends it to
           itself to get its own board's voltage"

           "In 1.4.0 there's now a 'fix' that ignores the 'T:' commands while the
           initial connect sequence is happening. This goes in this order:
            identity ("i")
            friend list . ("f" ... )
            battery query . (01x datagram, with 02x response)
           Only after all of those are complete will the 'T:' commands be processed"
            (Scott P via email)
        */
        // Since we don't have a battery, send a pre-recorded message
        apiResp = ("D:" + String(myUserID) + ",-157,02009F0F");
        break;
      }

      // sanity check length
      if (hexlen > 240)
        hexlen = 240;
      debugOutput("hexlen:" + String(hexlen));
      bp = (byte *) cmd.c_str() + parsePos + 1;

      memset(hexbuf, 0, 256);
      hexToBytes(bp, hexbuf, hexlen);

      // Look up destination ID
      if ( int_args[0] == 0xff ) {
        // Our special case - use broadcast key
        rippleSendDgm((char *) hexbuf, hexlen, 0xff, now());

      } else {
        // Find entry in addressbook
        int ix = aBookFindID(int_args[0]);
        if ( ix < 0 ) {
          debugOutput("Destination " + String(int_args[0]) + " not in contacts list");
          apiResp = String("E:");
          debugOutput("APIRES: " + apiResp);
          break;

        } else {
          rippleSendDgm((char *) hexbuf, hexlen, ix, now());
        }
      }
      break;

    case 't':
      // Send a Text Message
      //   t{to},{msg-id},{seq-no},{message-text}

      int_args[0] = cmd.substring(parsePos + 1).toInt();
      parsePos = cmd.indexOf(",", parsePos + 1);

      int_args[1] = cmd.substring(parsePos + 1).toInt() & 0xffffffff;
      parsePos = cmd.indexOf(",", parsePos + 1);

      int_args[2] = cmd.substring(parsePos + 1).toInt() & 0xff;
      parsePos = cmd.indexOf(",", parsePos + 1);

      if (parsePos > 0) {
        // Look up destination ID
        int abIx = 0xff;    // dest ID position in addressbook

        // (NONSTANDARD) - if destID=0xff, use global bcast key
        if ( int_args[0] == 0xff ) {
          abIx = 0xff;

        } else {
          // Find entry in addressbook
          abIx = aBookFindID(int_args[0]);

          if ( abIx < 0 ) {
            // Bail out if not found
            debugOutput("Destination " + String(int_args[0]) + " not in contacts list");
            apiResp = String("E:");
            break;
          }
        }
        ackID = rippleSendMsg(cmd.substring(parsePos + 1), abIx, int_args[1], int_args[2]);
        apiResp = String("O:" + String(ackID));
      }
      break;

    case 'p':
      if (inboxHead > 0) {
        debugOutput("Unread items: " + String(inboxHead));
        if (inbox[inboxHead - 1].type == 'A') {
          // Got an ACK
          apiResp = String("A:" + String(inbox[inboxHead - 1].msgid));
        } else if (inbox[inboxHead - 1].type == 'M') {
          // Got a message
          apiResp = String("M:" + String(inbox[inboxHead - 1].origin) + "," +
                           String(inbox[inboxHead - 1].msgid) + "," + String((char *) inbox[inboxHead - 1].body));
        }
      } else {
        debugOutput("Inbox EMPTY");
      }
      break;

    case 'd':
      if (inboxHead > 0) {
        inboxHead --;
        debugOutput("Unread items: " + String(inboxHead));
        // "T" means "inbox has changed" - EVEN IF it just became empty
        apiResp = String("T:"); // tickle
      }
      break;

    case '#':
      // Speed-dial a test message
      // Generate a medium-sized message for testing
      message = "UTC time is now " + String(hour()) + ":" + String(minute()) + ":" + String(second()) +
                ", millis are " + String(millis()) + " and we're still testing this thing.";
      debugOutput("Sending Quicktest message.");
      ackID = rippleSendMsg(message, 0xff, now(), 0);

      // O:{checksum}
      apiResp = "O:" + String(ackID);
      break;

    default:
      debugOutput("API[CMD] UNKNOWN: " + String(cmd.charAt(0)));
  }

  debugOutput("API[RES]: " + apiResp);
  return apiResp;
}

// Send unsolicited API notifications
void apiNotify(String apiNot)
{
  debugOutput("API[NOT]: " + apiNot);

  // send notification on serial
  Serial.print(apiNot + "\n");

#if BTS_ENABLED
  // if client connected on BT, send it there too
  if (SerialBT.hasClient())
    SerialBT.print(apiNot + "\n");
#endif

#if BLE_ENABLED
  // if client connected on BLE, ...
  if (bleDeviceConnected) {
    apiNot += "\n";
    pTxCharacteristic->setValue((byte *)apiNot.c_str(), apiNot.length());
    pTxCharacteristic->notify();
    debugOutput("BLE[TX]:" + String(apiNot.length()));
    delay(10);    // don't go crazy
  }
#endif
}


/************************************************************
   6) EEPROM / non-volatile data operations (OPTIONAL)

 ************************************************************/

#if USE_EEPROM
// Load configuration from EEPROM
void nvLoad()
{
  byte nvCRC8 = 0;
  if (EEPROM.begin(EEPROM_SIZE)) {
    debugOutput("Reading from EEPROM");
    // check version byte
    if (byte(EEPROM.read(0)) == NVCFGVERSION) {

      // read raw bytes into our structure
      for (int tempI = 0; tempI < NVCFGSIZE; tempI++) {
        ((byte *) &nvConfig)[tempI] = byte(EEPROM.read(tempI));
      }
      debugOutput(hexDump16((byte *) &nvConfig, 1));

      // check CRC
      for (int tempI = 0; tempI < NVCFGSIZE - 1; tempI++) {
        nvCRC8 = CRC8_add(((byte *) &nvConfig)[tempI], nvCRC8);
      }

      if (nvCRC8 != nvConfig.crc) {
        debugOutput("EEPROM CRC FAIL, want=" + String(nvConfig.crc, HEX) + ", have=" + String(nvCRC8, HEX));
        return;
      }

    } else {
      debugOutput("Unknown config version");
      return;
    }

    // Load values
    loraFR = nvConfig.freqKHz * 1000;
    myUserID = nvConfig.userID;
    groupID  = nvConfig.groupID;
    setTime(nvConfig.fakeRTC);
    debugOutput("Loaded NV config: freq=" + String(loraFR) + ", userID=" + String(myUserID) +
            ", group=" + String(groupID) + ", time=" + String(nvConfig.fakeRTC));

  } else {
    debugOutput("EEPROM not available");
    return;
  }  
}


// Save configuration to EEPROM
void nvSave()
{
  nvConfig.filever = NVCFGVERSION;
  nvConfig.freqKHz = int(loraFR/1000);
  nvConfig.userID = myUserID;
  nvConfig.groupID = groupID;
  nvConfig.fakeRTC = now();
  
  // calc CRC
  nvConfig.crc = 0;
  for (int tempI = 0; tempI < (NVCFGSIZE - 1); tempI++) {
    nvConfig.crc = CRC8_add(((byte *) &nvConfig)[tempI], nvConfig.crc);
  }

  debugOutput("Saving NV config, len=" + String(NVCFGSIZE) + ", CRC=" + String(nvConfig.crc, HEX));
  debugOutput(hexDump16((byte *) &nvConfig, 1));
  for (int tempI = 0; tempI < NVCFGSIZE; tempI++) {
    EEPROM.write(tempI, ((byte *) &nvConfig)[tempI]);
  }
  EEPROM.commit();
}
#endif
